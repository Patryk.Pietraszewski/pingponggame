import pygame
from pygame import *
import sys

pygame.init()
#wymiary okna oraz tło ekrany
Size=[1450,900]
Background=pygame.image.load("pong123.jpg")
BackgroundMenu=pygame.image.load("menipong.jpg")
#nazwa okienka
Window=pygame.display.set_mode(Size)
pygame.display.set_caption("2Players_PingPong")

####### Paletka
class Pallete:
    def __init__(self,HW,color,x,y,h,w,dif=1):
        self.HW=HW
        self.color=color
        self.rect= pygame.Rect(x,y,h,w)
        self.move = 0
        self.dif = dif
    # wygenerowanie paletki po podanych wartosciacch
    def draw_paletter(self):
        pygame.draw.rect(self.HW,self.color,self.rect)
    ##### ruch paletki
    def Move_left(self):
        if self.rect.x<0:
            self.rect.x=0
            self.move = 0
        else:
            self.rect.x-=8
            self.move = -self.dif
    def Move_right(self):
        if self.rect.x>1268:
            self.rect.x=1268
            self.move = 0
        else:
            self.rect.x += 8
            self.move = self.dif

Pallete_p1 = Pallete(Window, [255, 1, 4], 600, 810, 180, 30,dif=1) #czerwona
Pallete_p2 = Pallete(Window, [0, 1, 4], 600, 50, 180, 30,dif=-1) #Czarna
####### Sterowanie za pomoca Klawiszy

def events():
    if pygame.key.get_pressed()[pygame.K_a]== True:
        Pallete_p1.Move_left()
    if pygame.key.get_pressed()[pygame.K_d]==True:
        Pallete_p1.Move_right()
    if pygame.key.get_pressed()[pygame.K_LEFT]==True:
        Pallete_p2.Move_left()
    if pygame.key.get_pressed()[pygame.K_RIGHT]==True:
        Pallete_p2.Move_right()

######### Teksty  w grze
font_start=pygame.font.SysFont("Times New Roman",42)
font_surface=font_start.render("Naciśnij ENTER aby rozpocząć gre",True,(255, 255, 255))
options_surface=font_start.render("Naciśnij P aby zobaczyć sterowanie",True,(255, 255, 255))
controls_surface=font_start.render("ESC= Powrót  Sterowanie Gracz 1: (A,D) Gracz 2:(Lewa strzałka,Prawa strzałka)",True,(255, 255, 255))
pause_surface=font_start.render("Naciśnij SPACJA aby rozpoczać",True,(255, 255, 255))
escape_surface=font_start.render("ESC",True,(255, 255, 255))
End_game=font_start.render("Koniec Gry kliknij ENTER aby zagrać ponownie ESC wyjdź z gry",True,(255, 255, 255))
####### menu
def menu():
    while True:
        a=True
        for x in pygame.event.get():
            if x.type == QUIT or pygame.key.get_pressed()[pygame.K_ESCAPE]:
                pygame.quit()
                sys.exit()
        pygame.display.flip()
        Window.fill((0, 0, 0))
        Window.blit(BackgroundMenu, (0, 0))
        Window.blit(font_surface, (450, 450))
        Window.blit(options_surface, (450, 550))
        Window.blit(escape_surface, (50, 20))
        pygame.time.Clock().tick(60)
        pygame.display.update()
        if pygame.key.get_pressed()[13]==True:
            break
        if pygame.key.get_pressed()[pygame.K_p]:
            while a==True:
                for x in pygame.event.get():
                    if x.type == QUIT:
                        pygame.quit()
                        sys.exit()
                pygame.display.flip()
                Window.fill((0, 0, 0))
                Window.blit(BackgroundMenu,(0,0))
                Window.blit(controls_surface, (30,490))
                Window.blit(escape_surface,(50,20))
                pygame.time.Clock().tick(60)
                pygame.display.update()
                if pygame.key.get_pressed()[pygame.K_ESCAPE]:
                    break
####### Ending Screen
def ending():
    global Score1,Score2
    while True:
        for x in pygame.event.get():
            if x.type == QUIT or pygame.key.get_pressed()[pygame.K_ESCAPE]:
                pygame.quit()
                sys.exit()
            if pygame.key.get_pressed()[13]==True:
                GameMain()
                break
        pygame.display.flip()
        Window.fill((0, 0, 0))
        Window.blit(BackgroundMenu, (0, 0))
        Window.blit(escape_surface, (50, 20))
        Window.blit(End_game,(150, 450))
        if Score2==Score1:
            Window.blit(Draw, (600, 100))
        elif Score1 > Score2:
            Window.blit(Black_Win,(470, 100))
        else:
            Window.blit(Red_Win,(470,100))
        pygame.time.Clock().tick(60)
        pygame.display.update()

####### Wynik meczu
Score1=0 # Góra
Score2=0 #Dół
Score_font1=pygame.font.SysFont("New Times Roman",80)
Score_font1_surface=Score_font1.render(str(Score1),True,(255, 255, 255))
Score_font2_surface=Score_font1.render(str(Score2),True,(255, 255, 255))
#Informacja kto wygral
Draw=Score_font1.render("Remis",True,(255,255,255))
Black_Win=Score_font1.render("Wygrywa Gracz 2",True,(255,255,255))
Red_Win=Score_font1.render("Wygrywa Gracz 1",True,(255,255,255))
############# Piłka

class circle:

    def __init__(self,r,col):
        self.r = r
        self.color = col

class ball:

    def __init__(self,startX,startY,circle,HW):
        self.HW = HW
        self.x = startX
        self.y = startY
        self.cir = circle
        self.speed = [3.5, 4.5]
        self.minimalSpeedAction = [4.5, 5.5]
        self.speedAdd = [3.0, 3.5]
        self.lost = False
        self.canBeFatser = True
        self.fasterTimeInTicks = 10
        self.cFT = 0
    #fizyka piłki odbijanie sie
    def update(self,pl_1,pl_2):
        pygame.draw.circle(self.HW,self.cir.color,(int(self.x),int(self.y)),self.cir.r)
        size = self.HW.get_size()

        if (self.y+self.cir.r > pl_1.rect.y):
            if (pl_1.rect.x < self.x and pl_1.rect.x + pl_1.rect.width > self.x):
                self.speed = [self.speed[0]+pl_1.move,(self.speed[1]+pl_1.move)*-1]

        elif (self.y-self.cir.r-5 < pl_2.rect.y+pl_2.rect.height):
            if (pl_2.rect.x < self.x and pl_2.rect.x + pl_2.rect.width > self.x):
                self.speed = [(self.speed[1]+pl_2.move)*-1,self.speed[0]+pl_2.move]

        elif (self.x+self.cir.r-5 > size[0]):
            self.speed = [self.speed[0]*-1, self.speed[1]]

        elif (self.x-self.cir.r-5 < 0):
            self.speed = [self.speed[0]*-1, self.speed[1]]

        if(abs(self.speed[0]) < self.minimalSpeedAction[0]):
            if (self.speed[0] > 0):
                self.speed[0] = self.speed[0] + self.speedAdd[0]
            else:
                self.speed[0] = (self.speed[0] + self.speedAdd[0]) * -1
        if (abs(self.speed[1]) < self.minimalSpeedAction[1]):
            if (self.speed[1] > 0):
                self.speed[1] = self.speed[1] + self.speedAdd[1]
            else:
                self.speed[1] = (self.speed[1] + self.speedAdd[1]) * -1

        self.x = self.x + self.speed[0]
        self.y = self.y + self.speed[1]

        if (self.y < 0):
            self.lost = True
        if (self.y > size[1]):
            self.lost = True

############# Gra
def GameMain():
    global Score1,Score2
    Score1=0 # Góra
    Score2=0 #Dół
    Seconds = 180 #czas gry
    MAIN_BALL = ball(400, 400, circle(15, (234, 255, 1)), Window)
    Pauza=False
    EndGame=False
    while True:
        if Pauza==True:
            Window.blit(pause_surface, (499, 427))
            if pygame.key.get_pressed()[pygame.K_SPACE]:
                MAIN_BALL = ball(400, 400, circle(15, (234, 255, 1)), Window)
                Pauza=False
            if EndGame==True:
                ending()
        pygame.display.flip()
        Window.fill((0,0,0))
        Window.blit(Background,(0,0))
        Pallete_p1.draw_paletter()
        Pallete_p2.draw_paletter()
        Score_font1_surface = Score_font1.render(str(Score1), True, (255, 255, 255))
        Score_font2_surface = Score_font1.render(str(Score2), True, (255, 255, 255))
        Seconds_font=Score_font1.render(str(int(Seconds)),True, (255, 255, 255))
        ############ Reset Piłki i gol
        if (MAIN_BALL.lost == True and Pauza== False):
            if MAIN_BALL.y>0:
                Score1+=1
                Score_font1_surface = Score_font1.render(str(Score1), True, (255, 255, 255))
                Pauza=True
            if MAIN_BALL.y<0:
                Score2+=1
                Score_font2_surface = Score_font1.render(str(Score2), True, (255, 255, 255))
                Pauza=True
        ############ Timer
        if Pauza==False:
            Seconds-=1/45
            Seconds_font=Score_font1.render((str(int(Seconds))), True, (255, 255, 255))
        if Seconds<=0:
            EndGame=True
            Pauza=True
        if Seconds<100 and Seconds>=10:
            Window.blit(Seconds_font, (55, 400))
        elif Seconds>=100:
                Window.blit(Seconds_font ,(40,400))
        else:
            Window.blit(Seconds_font,(70,400))
        Window.blit(Score_font1_surface,(70,150))
        Window.blit(Score_font2_surface,(70,650))
        MAIN_BALL.update(Pallete_p1,Pallete_p2)

        for x in pygame.event.get():
            if x.type== QUIT:
                quit()

        events()
        pygame.time.Clock().tick(100)   #FPS
        pygame.display.update()
menu()
GameMain()
ending()